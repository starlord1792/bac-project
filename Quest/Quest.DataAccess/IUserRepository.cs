﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quest.BusinessObject.Entities;

namespace Quest.DataAccess
{
    public interface IUserRepository
    {
        IQueryable<User> UserSet { get; } 
    }
}
