﻿using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Quest.BusinessObject.Entities;

namespace Quest.DataAccess
{
    public partial class QuestContext : DbContext
    {
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<User> UserSet { get; set; }
        public virtual DbSet<Task> TaskSet { get; set; }
        public virtual DbSet<Level> LevelSet { get; set; }
        public virtual DbSet<Game> GameSet { get; set; }
        public virtual DbSet<New> NewSet { get; set; }
        public virtual DbSet<Team> TeamSet { get; set; }
        public virtual DbSet<Archive> ArchiveSet { get; set; }
        public virtual DbSet<Profile> ProfileSet { get; set; }
        public virtual DbSet<Contact> ContactSet { get; set; }
    }
}
