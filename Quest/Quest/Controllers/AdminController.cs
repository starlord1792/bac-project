﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Quest.Controllers
{
    public class AdminController : Controller
    {
        private INewRepository repository;

        public AdminController (INewRepository repo)
        {
            repository = repo;
        }
        
        public ViewResult Index()
        {
            return View(repository.News);
        }
    }
}