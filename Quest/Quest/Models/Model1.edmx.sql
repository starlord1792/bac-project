
-- --------------------------------------------------
-- Entity Designer DDL Script for SQL Server 2005, 2008, 2012 and Azure
-- --------------------------------------------------
-- Date Created: 04/16/2015 11:50:35
-- Generated from EDMX file: C:\Users\user\Documents\branches\Quest\Quest\Models\Model1.edmx
-- --------------------------------------------------

SET QUOTED_IDENTIFIER OFF;
GO
USE [QuestDatab];
GO
IF SCHEMA_ID(N'dbo') IS NULL EXECUTE(N'CREATE SCHEMA [dbo]');
GO

-- --------------------------------------------------
-- Dropping existing FOREIGN KEY constraints
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[FK_GameLevel]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[LevelSet] DROP CONSTRAINT [FK_GameLevel];
GO
IF OBJECT_ID(N'[dbo].[FK_LevelTask]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[TaskSet] DROP CONSTRAINT [FK_LevelTask];
GO
IF OBJECT_ID(N'[dbo].[FK_TeamUser]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserSet] DROP CONSTRAINT [FK_TeamUser];
GO
IF OBJECT_ID(N'[dbo].[FK_ArchiveTeam]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ArchiveSet] DROP CONSTRAINT [FK_ArchiveTeam];
GO
IF OBJECT_ID(N'[dbo].[FK_ArchiveGame]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[ArchiveSet] DROP CONSTRAINT [FK_ArchiveGame];
GO
IF OBJECT_ID(N'[dbo].[FK_UserProfile]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserSet] DROP CONSTRAINT [FK_UserProfile];
GO
IF OBJECT_ID(N'[dbo].[FK_Profile_inherits_User]', 'F') IS NOT NULL
    ALTER TABLE [dbo].[UserSet_Profile] DROP CONSTRAINT [FK_Profile_inherits_User];
GO

-- --------------------------------------------------
-- Dropping existing tables
-- --------------------------------------------------

IF OBJECT_ID(N'[dbo].[UserSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserSet];
GO
IF OBJECT_ID(N'[dbo].[TaskSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TaskSet];
GO
IF OBJECT_ID(N'[dbo].[LevelSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[LevelSet];
GO
IF OBJECT_ID(N'[dbo].[GameSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[GameSet];
GO
IF OBJECT_ID(N'[dbo].[NewSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[NewSet];
GO
IF OBJECT_ID(N'[dbo].[TeamSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[TeamSet];
GO
IF OBJECT_ID(N'[dbo].[ArchiveSet]', 'U') IS NOT NULL
    DROP TABLE [dbo].[ArchiveSet];
GO
IF OBJECT_ID(N'[dbo].[UserSet_Profile]', 'U') IS NOT NULL
    DROP TABLE [dbo].[UserSet_Profile];
GO

-- --------------------------------------------------
-- Creating all tables
-- --------------------------------------------------

-- Creating table 'UserSet'
CREATE TABLE [dbo].[UserSet] (
    [Login] nvarchar(20)  NOT NULL,
    [Password] nvarchar(20)  NOT NULL,
    [TeamId] int  NOT NULL,
    [ProfileP_Login] nvarchar(20)  NOT NULL,
    [ProfileP_Password] nvarchar(20)  NOT NULL
);
GO

-- Creating table 'TaskSet'
CREATE TABLE [dbo].[TaskSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Question] nvarchar(max)  NOT NULL,
    [Key] nvarchar(max)  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Level_Id] int  NOT NULL
);
GO

-- Creating table 'LevelSet'
CREATE TABLE [dbo].[LevelSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [GameId] int  NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Content] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'GameSet'
CREATE TABLE [dbo].[GameSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Name] nvarchar(max)  NOT NULL,
    [Backbone] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'NewSet'
CREATE TABLE [dbo].[NewSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Title] nvarchar(max)  NOT NULL,
    [Body] nvarchar(max)  NOT NULL
);
GO

-- Creating table 'TeamSet'
CREATE TABLE [dbo].[TeamSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [TeamName] nvarchar(max)  NOT NULL,
    [Active] bit  NOT NULL
);
GO

-- Creating table 'ArchiveSet'
CREATE TABLE [dbo].[ArchiveSet] (
    [Id] int IDENTITY(1,1) NOT NULL,
    [Result] nvarchar(max)  NOT NULL,
    [Team_Id] int  NOT NULL,
    [Game_Id] int  NOT NULL
);
GO

-- Creating table 'UserSet_Profile'
CREATE TABLE [dbo].[UserSet_Profile] (
    [Id] nvarchar(max)  NOT NULL,
    [Name] nvarchar(20)  NOT NULL,
    [SecondName] nvarchar(20)  NOT NULL,
    [Age] int  NOT NULL,
    [Gender] bit  NOT NULL,
    [Photo] tinyint  NOT NULL,
    [Contacts] nvarchar(100)  NOT NULL,
    [Skills] nvarchar(max)  NOT NULL,
    [Equipment] nvarchar(200)  NOT NULL,
    [AboutYourself] nvarchar(300)  NOT NULL,
    [Transport] nvarchar(100)  NOT NULL,
    [Login] nvarchar(20)  NOT NULL,
    [Password] nvarchar(20)  NOT NULL
);
GO

-- --------------------------------------------------
-- Creating all PRIMARY KEY constraints
-- --------------------------------------------------

-- Creating primary key on [Login], [Password] in table 'UserSet'
ALTER TABLE [dbo].[UserSet]
ADD CONSTRAINT [PK_UserSet]
    PRIMARY KEY CLUSTERED ([Login], [Password] ASC);
GO

-- Creating primary key on [Id] in table 'TaskSet'
ALTER TABLE [dbo].[TaskSet]
ADD CONSTRAINT [PK_TaskSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'LevelSet'
ALTER TABLE [dbo].[LevelSet]
ADD CONSTRAINT [PK_LevelSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'GameSet'
ALTER TABLE [dbo].[GameSet]
ADD CONSTRAINT [PK_GameSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'NewSet'
ALTER TABLE [dbo].[NewSet]
ADD CONSTRAINT [PK_NewSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'TeamSet'
ALTER TABLE [dbo].[TeamSet]
ADD CONSTRAINT [PK_TeamSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Id] in table 'ArchiveSet'
ALTER TABLE [dbo].[ArchiveSet]
ADD CONSTRAINT [PK_ArchiveSet]
    PRIMARY KEY CLUSTERED ([Id] ASC);
GO

-- Creating primary key on [Login], [Password] in table 'UserSet_Profile'
ALTER TABLE [dbo].[UserSet_Profile]
ADD CONSTRAINT [PK_UserSet_Profile]
    PRIMARY KEY CLUSTERED ([Login], [Password] ASC);
GO

-- --------------------------------------------------
-- Creating all FOREIGN KEY constraints
-- --------------------------------------------------

-- Creating foreign key on [GameId] in table 'LevelSet'
ALTER TABLE [dbo].[LevelSet]
ADD CONSTRAINT [FK_GameLevel]
    FOREIGN KEY ([GameId])
    REFERENCES [dbo].[GameSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_GameLevel'
CREATE INDEX [IX_FK_GameLevel]
ON [dbo].[LevelSet]
    ([GameId]);
GO

-- Creating foreign key on [Level_Id] in table 'TaskSet'
ALTER TABLE [dbo].[TaskSet]
ADD CONSTRAINT [FK_LevelTask]
    FOREIGN KEY ([Level_Id])
    REFERENCES [dbo].[LevelSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_LevelTask'
CREATE INDEX [IX_FK_LevelTask]
ON [dbo].[TaskSet]
    ([Level_Id]);
GO

-- Creating foreign key on [TeamId] in table 'UserSet'
ALTER TABLE [dbo].[UserSet]
ADD CONSTRAINT [FK_TeamUser]
    FOREIGN KEY ([TeamId])
    REFERENCES [dbo].[TeamSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_TeamUser'
CREATE INDEX [IX_FK_TeamUser]
ON [dbo].[UserSet]
    ([TeamId]);
GO

-- Creating foreign key on [Team_Id] in table 'ArchiveSet'
ALTER TABLE [dbo].[ArchiveSet]
ADD CONSTRAINT [FK_ArchiveTeam]
    FOREIGN KEY ([Team_Id])
    REFERENCES [dbo].[TeamSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ArchiveTeam'
CREATE INDEX [IX_FK_ArchiveTeam]
ON [dbo].[ArchiveSet]
    ([Team_Id]);
GO

-- Creating foreign key on [Game_Id] in table 'ArchiveSet'
ALTER TABLE [dbo].[ArchiveSet]
ADD CONSTRAINT [FK_ArchiveGame]
    FOREIGN KEY ([Game_Id])
    REFERENCES [dbo].[GameSet]
        ([Id])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_ArchiveGame'
CREATE INDEX [IX_FK_ArchiveGame]
ON [dbo].[ArchiveSet]
    ([Game_Id]);
GO

-- Creating foreign key on [ProfileP_Login], [ProfileP_Password] in table 'UserSet'
ALTER TABLE [dbo].[UserSet]
ADD CONSTRAINT [FK_UserProfile]
    FOREIGN KEY ([ProfileP_Login], [ProfileP_Password])
    REFERENCES [dbo].[UserSet_Profile]
        ([Login], [Password])
    ON DELETE NO ACTION ON UPDATE NO ACTION;
GO

-- Creating non-clustered index for FOREIGN KEY 'FK_UserProfile'
CREATE INDEX [IX_FK_UserProfile]
ON [dbo].[UserSet]
    ([ProfileP_Login], [ProfileP_Password]);
GO

-- Creating foreign key on [Login], [Password] in table 'UserSet_Profile'
ALTER TABLE [dbo].[UserSet_Profile]
ADD CONSTRAINT [FK_Profile_inherits_User]
    FOREIGN KEY ([Login], [Password])
    REFERENCES [dbo].[UserSet]
        ([Login], [Password])
    ON DELETE CASCADE ON UPDATE NO ACTION;
GO

-- --------------------------------------------------
-- Script has ended
-- --------------------------------------------------