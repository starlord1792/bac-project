﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Quest.BusinessObject.Entities;

namespace Quest.DataAccess.Implementation
{
    public class UserRepository : IUserRepository
    {
        private QuestContext _context = new QuestContext();

        public IQueryable<User> UserSet {
            get { return _context.UserSet; }
        }
    }
}
