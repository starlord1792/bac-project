﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace Quest.GlobalModel
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Infrastructure;
    
    public partial class Model1Container : DbContext
    {
        public Model1Container()
            : base("name=Model1Container")
        {
        }
    
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            throw new UnintentionalCodeFirstException();
        }
    
        public virtual DbSet<User> UserSet { get; set; }
        public virtual DbSet<Task> TaskSet { get; set; }
        public virtual DbSet<Level> LevelSet { get; set; }
        public virtual DbSet<Game> GameSet { get; set; }
        public virtual DbSet<New> NewSet { get; set; }
        public virtual DbSet<Team> TeamSet { get; set; }
        public virtual DbSet<Archive> ArchiveSet { get; set; }
        public virtual DbSet<Profile> ProfileSet { get; set; }
        public virtual DbSet<Contact> ContactSet { get; set; }
    }
}
